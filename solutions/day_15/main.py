"""
Day 15 Exercise - Cavern
"""
from time import monotonic
from collections.abc import Callable
from dataclasses import dataclass, field
from typing import Generator, ClassVar
from itertools import count
from heapq import heappush, heappop
import logging
import sys


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("Cavern.log"),
        logging.StreamHandler(sys.stdout)
    ]
)


def timer_func(func) -> Callable:
    """
    Timer for functions
    :param func: a function to be timed
    :return: a decorator for timing the function
    """
    def wrap_func(*args, **kwargs):
        t_1 = monotonic()
        result = func(*args, **kwargs)
        t_2 = monotonic()
        logging.info(f'Function {func.__name__!r} '
                     f'executed in {(t_2 - t_1):.5}s')
        return result
    return wrap_func


def read_input(puzzle_file: str) -> list[list[int]]:
    """
    Reads puzzle input
    :param puzzle_file: name of puzzle_input file
    :return: list[list[int]] cavern
    """
    with open(puzzle_file, "r", encoding="utf-8") as file:
        puzzle_input = file.read().splitlines()
    full_cavern = [[int(i) for i in row] for row in puzzle_input]
    return full_cavern


def extend_input(puzzle_file: str) -> list[list[int]]:
    """
    Returns the full cave
    :param puzzle_file: name od puzzle input file
    :return: return the full cave (original * 5)
    """
    with open(puzzle_file, "r", encoding="utf-8") as file:
        puzzle_input = file.read().splitlines()
    full_cavern = [[int(i) for i in row] for row in puzzle_input]
    full_cavern_extended = []
    for row_index in range(len(full_cavern)*5):
        full_cavern_extended_row = []
        for column_index in range(len(full_cavern[0])*5):
            element = \
                full_cavern[row_index % len(full_cavern)][column_index % len(full_cavern[0])] + \
                int(column_index/int(len(full_cavern[0]))) + int(row_index/int(len(full_cavern)))
            if element < 10:
                full_cavern_extended_row.append(element)
            else:
                full_cavern_extended_row.append(element - 9)
        full_cavern_extended.append(full_cavern_extended_row)
    return full_cavern_extended


@dataclass
class PriorityQueue:
    """
    An implementation of a custom priority queue using heapq library

    Attributes
    ----------
    - heap: list:
        list of items arranged in a heap
    - entry_finder: dict:
        mapping of items to entries
    - counter: Generator:
        a counter for recording the order of adding items
    - removed: ClassVer[str]:
        Class Variable, placeholder for a removed item

    Methods
    -------
    - add_item(item: tuple, priority:int = 0) -> None:
        Add a new item into the priority queue
        or update the priority of an existing item
    - remove_item(item: tuple) -> None:
        Mark an existing item as REMOVED.
        Raise KeyError if not found.
    - pop_item() -> tuple:
        Remove and return the lowest priority item.
        Raise KeyError if empty.
    """

    heap: list = field(default_factory=list)
    entry_finder: dict = field(default_factory=dict)
    counter: Generator = count()
    removed: ClassVar[str] = '<removed-item>'

    def add_item(self, item: tuple, priority: int = 0) -> None:
        """
        Add a new item or update the priority of an existing item
        :param item: item to be added
        :param priority: priority of item
        :return: None
        """
        if item in self.entry_finder:
            if self.entry_finder[item][0] >= priority:
                self.remove_item(item)
            else:
                return
        next_count = next(self.counter)
        entry = [priority, next_count, item]
        self.entry_finder[item] = entry
        heappush(self.heap, entry)

    def remove_item(self, item: tuple) -> None:
        """
        Mark an existing item as REMOVED.
        Raise KeyError if not found.
        :param item: item to be removed
        :return: None
        """
        entry = self.entry_finder.pop(item)
        entry[-1] = self.removed

    def pop_item(self) -> tuple:
        """
        Remove and return the lowest priority item.
        Raise KeyError if empty.
        :return: item with the lowest priority
        """
        while self.heap:
            item = heappop(self.heap)[2]
            if item is not self.removed:
                return item
        raise KeyError('tried to pop from an empty priority queue')


def calc_total_risk(cavern: list[list[int]]) -> int:
    """
    Calculate the total risk of going through the cavern using
    a Dijkstra's algorithm with a priority queue
    :param cavern: output of read_input
    :return: the total risk of the cavern
    """
    visited = set()
    queue = PriorityQueue([], {})
    queue.add_item((0, 0), 0)  # treat risk as priority
    while (len(cavern) - 1, len(cavern[0]) - 1) not in visited:
        current_point = queue.pop_item()
        # check for neighbours
        neighbours = [
            (current_point[0], current_point[1] - 1)
            if current_point[1] - 1 >= 0 else False,  # up
            (current_point[0] + 1, current_point[1])
            if current_point[0] + 1 < len(cavern[0]) else False,  # right
            (current_point[0], current_point[1] + 1)
            if current_point[1] + 1 < len(cavern) else False,  # down
            (current_point[0] - 1, current_point[1])
            if current_point[0] - 1 >= 0 else False]  # left

        # update priority queue
        for neighbour in neighbours:
            if neighbour and neighbour not in visited:
                risk = cavern[neighbour[1]][neighbour[0]]
                queue.add_item(neighbour, risk + queue.entry_finder[current_point][0])

        visited.add(current_point)

    total_risk = queue.entry_finder[(len(cavern) - 1, len(cavern[0]) - 1)][0]
    logging.info(f"The total risk is {total_risk}")
    return total_risk


@timer_func
def part_1() -> int:
    """
    :return: Total risk
    """
    logging.info("Calculating the total risk...")
    cavern = read_input("puzzle_input.txt")
    total_risk = calc_total_risk(cavern)
    logging.info(f"Solution to part 1 is {total_risk}")
    return total_risk


@timer_func
def part_2() -> int:
    """
    :return: Total Risk
    """
    logging.info("Calculating the total risk...")
    extended_cavern = extend_input("puzzle_input.txt")
    total_risk = calc_total_risk(extended_cavern)
    logging.info(f"Solution to part 2 is {total_risk}")
    return total_risk


if __name__ == '__main__':
    part_1()
    part_2()
