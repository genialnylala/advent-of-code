"""
Day 21 Exercise - Dirac Dice
"""
from time import monotonic
from collections.abc import Callable, Iterator
from collections import Counter, defaultdict
from dataclasses import dataclass
from itertools import count, product
from typing import ClassVar, Union
import logging
import sys


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("Dirac_Dice.log"),
        logging.StreamHandler(sys.stdout)
    ]
)


def timer_func(func) -> Callable:
    """
    Timer for functions
    :param func: a function to be timed
    :return: a decorator for timing the function
    """
    def wrap_func(*args, **kwargs):
        t_1 = monotonic()
        result = func(*args, **kwargs)
        t_2 = monotonic()
        logging.info(f'Function {func.__name__!r} '
                     f'executed in {(t_2 - t_1):.5}s')
        return result
    return wrap_func


def read_input(puzzle_file: str) -> (int, int):
    """
    Reads puzzle input
    :param puzzle_file: name of puzzle_input file
    :return: starting positions of player 1 and player 2
    """
    with open(puzzle_file, "r", encoding="utf-8") as file:
        puzzle_input = file.read().splitlines()
    p1_start, p2_start = (int(starting_position[28:]) for starting_position in puzzle_input)
    return p1_start, p2_start


@dataclass
class DeterministicDie:
    """
    Class representing a deterministic die that counts from 1 to 100

    Attributes
    ----------
    - die: Iterator
        an iterator that counts from 1 upwards
    - last_roll: int
        the last value of die
    - tot_roll: int
        total number that roll() was called

    Methods
    -------
    - roll() -> int:
        returns the result of the next roll
    - roll3() -> int
        returns the sum of the next three rolls

    """
    die: Iterator = count(1)
    last_roll: int = 0
    tot_roll: int = 0

    def roll(self) -> int:
        """ :return: the result of the next roll  """
        self.tot_roll += 1
        if self.last_roll < 101:
            return next(self.die)
        self.die = count(1)
        return next(self.die)

    def roll3(self) -> int:
        """ :return: the sum of the next three rolls"""
        return sum((self.roll(), self.roll(), self.roll()))


@dataclass
class Player:
    """
    Class representing a person playing dice

    Attributes
    ---------
    - player_no: int
        identification number of the player
    - position: int
        position of player on board
    - score: int
        score of the player
    - win: ClassVar[int]
        Class Variable. Gives the player_no of the player,
        that has won the game

    Methods
    ------
    - play(outcome):
        simulates a round of the game for a single player
    """
    player_no: int
    position: int
    score: int = 0
    win: ClassVar[int] = 0

    def play(self, outcome) -> Union[bool, int]:
        """
        simulates a round of the game for a single player
        :param outcome: the outcome of the dice roll
        :return: player_no of the player that won the game OR
                False, if nobody has won yet
        """
        self.position = (self.position - 1 + outcome) % 10 + 1
        self.score += self.position
        if self.score < 1000:
            return False
        Player.win = self.player_no
        return self.player_no


def dirac_game(p1_pos: int, p2_pos: int) -> int:
    """
    Play a game of dirac dice
    :param p1_pos: player 1 starting position
    :param p2_pos: player 2 starting position
    :return: no of universes in which the winning player wins
    """

    # get the sum of all possible die throw combinations
    # and their frequencies
    frequencies = Counter([sum(combination) for combination in product((1, 2, 3), repeat=3)])

    # The keys of the player_combinations dict are:
    # (player 1 position, player 1 score, player 2 position, player 2 score)
    # the values are the number of universes in which this
    # particular combination of positions and scores occurs
    player_combinations = {(p1_pos, 0, p2_pos, 0): 1}

    # number of times each player wins
    wins = [0, 0]

    # repeat until games in all universes are finished
    while player_combinations:

        # player 1 turn
        new_combinations = defaultdict(lambda: 0)
        for combination, no_occurance in player_combinations.items():

            # player one casts die three times, creating
            # no_new_universes new universes
            for die_outcome, no_new_universes in frequencies.items():
                new_position_1 = (combination[0] - 1 + die_outcome) % 10 + 1
                new_score_1 = combination[1] + new_position_1
                if new_score_1 < 21:
                    new_combinations[(new_position_1, new_score_1,
                                      combination[2], combination[3])] \
                        += no_new_universes * no_occurance
                else:
                    wins[0] += no_new_universes * no_occurance
        player_combinations = new_combinations

        # player 2 turn
        new_combinations = defaultdict(lambda: 0)
        for combination, no_occurance in player_combinations.items():
            # player two casts die three times, creating
            # no_new_universes new universes
            for die_outcome, no_new_universes in frequencies.items():
                new_position_2 = (combination[2] - 1 + die_outcome) % 10 + 1
                new_score_2 = combination[3] + new_position_2
                if new_score_2 < 21:
                    new_combinations[(combination[0], combination[1],
                                      new_position_2, new_score_2)] \
                        += no_new_universes * no_occurance
                else:
                    wins[1] += no_new_universes * no_occurance
        player_combinations = new_combinations

    # get maximum score
    result = max(wins)
    return result


@timer_func
def part_1() -> int:
    """
    :return: Product of score of losing player and the number of times
            the die was rolled during the game
    """
    logging.info("Calculating Product of score of losing player and the number of times \
the die was rolled during the game...")
    p1_start, p2_start = read_input("puzzle_input.txt")

    die = DeterministicDie()
    players = (Player(1, p1_start), Player(2, p2_start))

    # play game
    while True:
        if players[0].play(die.roll3()):
            break
        if players[1].play(die.roll3()):
            break
    players_number = [0, 1]
    players_number.remove(Player.win - 1)
    result = players[players_number[0]].score * die.tot_roll

    logging.info(f"Solution to part 1 is {result}")
    return result


@timer_func
def part_2() -> int:
    """
    :return: number of universes in which the winner player wins
    """
    logging.info("Calculating the number of universes in which the winner player wins...")
    p1_pos, p2_pos = read_input("puzzle_input.txt")
    result = dirac_game(p1_pos, p2_pos)
    logging.info(f"Solution to part 2 is {result}")
    return result


if __name__ == '__main__':
    part_1()
    part_2()
