"""
Day 16 Exercise - Packet Decoder
"""
from time import monotonic
from collections.abc import Callable
from collections import deque
from dataclasses import dataclass
from abc import ABC, abstractmethod
from typing import Union
import logging
import sys

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("Transmission.log"),
        logging.StreamHandler(sys.stdout)
    ]
)


def timer_func(func) -> Callable:
    """
    Timer for functions
    :param func: a function to be timed
    :return: a decorator for timing the function
    """

    def wrap_func(*args, **kwargs):
        t_1 = monotonic()
        result = func(*args, **kwargs)
        t_2 = monotonic()
        logging.info(f'Function {func.__name__!r} '
                     f'executed in {(t_2 - t_1):.5}s')
        return result

    return wrap_func


def read_input(puzzle_file: str) -> deque:
    """
    Reads puzzle input
    :param puzzle_file: name of puzzle_input file
    :return: Full Transmission
    """
    with open(puzzle_file, "r", encoding="utf-8") as file:
        puzzle_input = [format(int(hex_num, 16), 'b').zfill(4) for hex_num in file.read().strip()]
    transmission = deque("".join(puzzle_input))
    return transmission


@dataclass
class Packet(ABC):
    """
    Abstract Packet Class skeleton to base literal value packets
    and operator packet on

    Attributes
    ---------
    - version: int
        the packet version
    - type_id: int
        the packet type ID
    """
    version: int
    type_id: int

    @abstractmethod
    def sum_versions(self):
        """ skeleton for child class"""

    @abstractmethod
    def operate(self):
        """ skeleton for child class"""


@dataclass
class LiteralValue(Packet):
    """
    Literal Value Packet, inheriting from Packet

    Attributes
    ----------
    - value: int
        the literal value of the packet

    Methods
    ------
    - sum_versions() -> int:
        returns the version of the packet
    - operate() -> int
        returns the value of the packet

    """
    value: int

    def sum_versions(self) -> int:
        """ :return: the version of the packet """
        return self.version

    def operate(self) -> int:
        """ :return: the value of the packet """
        return self.value


@dataclass
class OperatorPacket(Packet):
    """
    Operator Packet, inheriting from Packet

    Attributes
    ----------
    - subpackets: list
        A list of the subpackets of the operator packet

    Methods
    ------
    - sum_versions() -> int:
        returns the sum of versions of the package and all subpackets
    - operate() -> int
        returns the result of operations on the subpackets
    """
    subpackets: list

    def sum_versions(self) -> int:
        """ :return: the sum of versions of the package and all subpackets """
        return self.version + sum(packet.sum_versions() for packet in self.subpackets)

    def operate(self) -> int:
        """ :return: the result of operations on the subpackets """
        if self.type_id == 0:  # sum
            result = sum(packet.operate() for packet in self.subpackets)
        elif self.type_id == 1:  # product
            product = 1
            for i in [packet.operate() for packet in self.subpackets]:
                product *= i
            result = product
        elif self.type_id == 2:  # minimum
            result = min(packet.operate() for packet in self.subpackets)
        elif self.type_id == 3:  # maximum
            result = max(packet.operate() for packet in self.subpackets)
        elif self.type_id == 5:  # greater than
            packets = [packet.operate() for packet in self.subpackets]
            if packets[0] > packets[1]:
                result = 1
            else:
                result = 0
        elif self.type_id == 6:  # less than
            packets = [packet.operate() for packet in self.subpackets]
            if packets[0] < packets[1]:
                result = 1
            else:
                result = 0
        elif self.type_id == 7:  # equal to
            packets = [packet.operate() for packet in self.subpackets]
            if packets[0] == packets[1]:
                result = 1
            else:
                result = 0

        return result


@dataclass
class Parser:
    """
    A class to parse the transmission

    Attributes
    ----------
    - transmission: deque
        the transmission in binary in deque format
    - transmission_length: int
        the length of the transmission
    - index: int
        the number of bits that have been read so far

    Methods
    -------
    - get_bits(number_of_bits) -> str:
        gets the next batch of bits from the transmission
    - get_header() -> tuple:

    """

    transmission: deque
    transmission_length: int = 0
    index: int = 0

    def get_bits(self, number_of_bits) -> str:
        """
        gets the next batch of bits from the transmission
        :param number_of_bits: required length of the batch
        :return: batch of bits
        """
        try:
            self.index += number_of_bits
            return "".join([self.transmission.popleft() for _ in range(number_of_bits)])
        except IndexError as exc:
            logging.info("Transmission over")
            raise Exception("<transmission-over>") from exc

    def get_header(self) -> tuple:
        """
        :return: header of the next package
        """
        version = int(self.get_bits(3), 2)
        type_id = int(self.get_bits(3), 2)
        return version, type_id

    def get_packet(self) -> Union[LiteralValue, OperatorPacket]:
        """
        General method which to get the next packet in the transmission
        :return: the next packet in transmission
        """
        version, type_id = self.get_header()
        if type_id == 4:
            return LiteralValue(version, type_id, self.decode_value())
        return OperatorPacket(version, type_id, self.get_subpackets())

    def get_subpackets(self) -> list:
        """
        Gets subpackets from the operator packet
        :return: all subpackets of a given operator packet
        """
        subpackets = []
        length_type_id = self.get_bits(1)
        if length_type_id == '0':  # length type packet
            total_length = int(self.get_bits(15), 2)
            end_index = self.index + total_length
            while self.index < end_index:
                subpackets.append(self.get_packet())
        else:  # subpackets type packet
            no_subpackets = int(self.get_bits(11), 2)
            for _ in range(no_subpackets):
                subpackets.append(self.get_packet())
        return subpackets

    def decode_value(self) -> int:
        """
        Decodes the value passed on from the transmission
        :return: decoded value in decimal format
        """
        value_bits = []
        value_bit = self.get_bits(5)
        while value_bit[0] == '1':
            value_bits.append(value_bit[1:])
            value_bit = self.get_bits(5)

        value_bits.append(value_bit[1:])
        # convert to decimal number
        num = "".join(value_bits)
        value = int(num, 2)
        return value

    def parse_transmission(self):
        """
        Parse the next packet in the transmission
        :return: the next packet
        """
        self.transmission_length = len(self.transmission)
        return self.get_packet()


@timer_func
def part_1() -> int:
    """
    :return: the sum of all versions
    """
    logging.info("Calculating sum of all version...")
    transmission = read_input("puzzle_input.txt")
    parser = Parser(transmission)
    full_packet = parser.parse_transmission()
    sum_v = full_packet.sum_versions()

    logging.info(f"Solution to part 1 is {sum_v}")
    return sum_v


@timer_func
def part_2() -> int:
    """
    :return: The result of the transmission
    """
    logging.info("Calculating the result of the transmission...")
    transmission = read_input("puzzle_input.txt")
    parser = Parser(transmission)
    full_packet = parser.parse_transmission()
    result = full_packet.operate()

    logging.info(f"Solution to part 2 is {result}")
    return result


if __name__ == '__main__':
    part_1()
    part_2()
