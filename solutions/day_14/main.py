"""
Day 14 Exercise - Extended Polymerization
"""
from time import monotonic_ns
from collections.abc import Callable
from collections import Counter, defaultdict
from copy import copy
from dataclasses import dataclass
from typing import NoReturn
import logging
import sys


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("Transparent_Origami.log"),
        logging.StreamHandler(sys.stdout)
    ]
)


def timer_func(func) -> Callable:
    """
    Timer for functions
    :param func: a function to be timed
    :return: a decorator for timing the function
    """
    def wrap_func(*args, **kwargs):
        t_1 = monotonic_ns()
        result = func(*args, **kwargs)
        t_2 = monotonic_ns()
        logging.info(f'Function {func.__name__!r} '
                     f'executed in {(t_2 - t_1):,d}ns')
        return result
    return wrap_func


def read_input(puzzle_file: str) -> (str, dict):
    """
    Reads puzzle input
    :param puzzle_file: name of puzzle_input file
    :return:
        polymer_starter - initial polymer
        polymerization_rule - rules applied at which polymerization cycle
    """
    with open(puzzle_file, "r", encoding="utf-8") as file:
        puzzle_input = file.read()
    puzzle_input = puzzle_input.split("\n\n")
    polymer_starter = puzzle_input[0].split("\n")[0]
    # converting polymerization rules into a comfortable format
    polymerization_rules = puzzle_input[1].split("\n")
    polymerization_rules = {rule[:2]: rule[-1] for rule in polymerization_rules}
    for i, j in polymerization_rules.items():
        polymerization_rules[i] = [i[0]+j, j+i[1]]
    return polymer_starter, polymerization_rules


@dataclass
class Polymer:
    """
    A Class representing a chemical polymer

    Attributes
    ----------
    - starter: str
        the initial polymer chain
    - rules: dict
        the rules for further polymerization

    Methods
    -------
    polymerize(no_steps: int)
        calculates the number of different monomers throughout
        no_steps polymerization steps

    calc_diff_max_min()
        returns the difference between the number of the most common
        monomer and the least common monomer
    """
    def __init__(self, starter, rules):
        self.starter: str = starter
        self.rules: dict = rules
        self._combinations, self._comb_counter, self._letters_counter = \
            self.prepare_polymerization(starter)

    @staticmethod
    def prepare_polymerization(starter: str) -> (list, dict, defaultdict):
        """
        generates the _combinations, _comb_counter, _letters_counter parameters
        :param starter: starting polymer
        :return:
            - combinations: all different arrangement of 2 letter polymers
                            in the starter polymer
            - comb_counter: counter of how often which combination occurs
                            in the polymer
            - letters_counter: counter of how often which individual letters
                            occur in the polymer
        """
        combinations = [starter[i:i + 2] for i in range(len(starter) - 1)]
        comb_counter = dict(Counter(combinations))
        letters_counter = defaultdict(lambda: 0)
        for i, j in dict(Counter(starter)).items():
            letters_counter[i] = j
        return combinations, comb_counter, letters_counter

    def polymerize(self, no_steps: int) -> NoReturn:
        """
        calculates the number of different monomers throughout
        no_steps polymerization steps
        :param no_steps: number of polymerization cycles to be executed
        :return: NoReturn
        """
        for _ in range(no_steps):
            c_temp = defaultdict(lambda: 0)
            for pre, post in self.rules.items():
                try:
                    c_temp[post[0]] += self._comb_counter[pre]
                    c_temp[post[1]] += self._comb_counter[pre]
                    self._letters_counter[post[0][1]] += self._comb_counter[pre]
                except KeyError:
                    pass
            self._comb_counter = copy(c_temp)

    def calc_diff_max_min(self):
        """
        :return: the difference between the number of the most common
        monomer and the least common monomer
        """
        return max(self._letters_counter.values())-min(self._letters_counter.values())


@timer_func
def part_1() -> int:
    """
    :return: Difference between the number of the most and least
            common letters in the polymer
    """
    logging.info("Calculating polymerization...")
    starter, rules = read_input("puzzle_input.txt")
    my_polymer = Polymer(starter, rules)
    my_polymer.polymerize(10)
    diff = my_polymer.calc_diff_max_min()
    logging.info(f"Solution to part 1 is {diff}")

    return diff


@timer_func
def part_2() -> int:
    """
    :return: Difference between the number of the most and least
            common letters in the polymer
    """
    logging.info("Calculating polymerization...")
    starter, rules = read_input("puzzle_input.txt")
    my_polymer = Polymer(starter, rules)
    my_polymer.polymerize(40)
    diff = my_polymer.calc_diff_max_min()
    logging.info(f"Solution to part 2 is {diff}")

    return diff


if __name__ == '__main__':
    part_1()
    part_2()
