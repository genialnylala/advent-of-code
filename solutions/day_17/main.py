"""
Day 17 Exercise - Probe Trajectories
"""
from time import monotonic
from collections.abc import Callable
from itertools import count
from math import sqrt
import logging
import sys


logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("Trick_Shot.log"),
        logging.StreamHandler(sys.stdout)
    ]
)


def timer_func(func) -> Callable:
    """
    Timer for functions
    :param func: a function to be timed
    :return: a decorator for timing the function
    """
    def wrap_func(*args, **kwargs):
        t_1 = monotonic()
        result = func(*args, **kwargs)
        t_2 = monotonic()
        logging.info(f'Function {func.__name__!r} '
                     f'executed in {(t_2 - t_1):.5}s')
        return result
    return wrap_func


def read_input(puzzle_file: str) -> (int, int, int, int):
    """
    Reads puzzle input
    :param puzzle_file: name of puzzle_input file
    :return: x_min, x_max, y_min, y_max
    """
    with open(puzzle_file, "r", encoding="utf-8") as file:
        puzzle_input = file.read()[15:-1].split(" ")
    puzzle_input[0] = puzzle_input[0][:-1]
    puzzle_input[1] = puzzle_input[1][2:]
    puzzle_input = [list(map(int, i.split(".."))) for i in puzzle_input]
    return puzzle_input[0][0], puzzle_input[0][1], puzzle_input[1][0], puzzle_input[1][1]


def check_trajectory(v_y: int, v_x: int, x_min: int, x_max: int, y_min: int, y_max: int) -> bool:
    """
    By writing the problem down as a set of difference equations
    we are able to calculate the trajectory of the probe
    :param v_y: initial vertical velocity
    :param v_x: initial horizontal velocity
    :param x_min: minimum horizontal coordinate of target
    :param x_max: maximum horizontal coordinate of target
    :param y_min: minimum vertical coordinate of target
    :param y_max: maximum vertical coordinate of target
    :return: True if trajectory goes through target,
             False if trajectory doesn't go into target
    """
    t_counter = count(1)
    y_coord, x_coord = (0, 0)
    while x_coord <= x_max and y_coord >= y_min:
        t_value = next(t_counter)

        # Calculate y coordinates
        y_coord = -t_value * (t_value + 1) / 2 + (v_y + 1) * t_value

        # Calculate x coordinates
        if v_x == 0:
            x_coord = 0
        elif v_x > 0:
            if t_value < v_x:
                x_coord = -t_value * (t_value + 1) / 2 + (v_x + 1) * t_value
            else:
                pass
        else:
            if t_value > v_x:
                x_coord = t_value * (t_value + 1) / 2 + (v_x + 1) * t_value
            else:
                pass

        # check if in target
        if x_min <= x_coord <= x_max and y_min <= y_coord <= y_max:
            return True
    return False


@timer_func
def part_1() -> int:
    """
    :return: Highest possible point
    """
    # since x and y are independent of each other,
    # and due to symmetry, the downwards velocity v_y_t of the probe at y = 0, t > 0
    # must be equal to its initial upwards velocity v_y when starting at t,x,y = 0
    # the maximum downwards velocity must be v_y_t_max = y_min + 1
    # (assuming target below x-axis)
    # hence v_y_max = |y_min + 1| and the highest point
    # is simply the sum 1+2+...+v_y_max = v_y_max*(v_y_max+1)/2
    logging.info("Calculating highest point...")
    y_min = read_input("puzzle_input.txt")[2]
    v_y_max = abs(y_min + 1)
    highest_point = int(v_y_max * (v_y_max + 1) / 2)
    logging.info(f"Solution to part 1 is {highest_point}")
    return highest_point


@timer_func
def part_2() -> int:
    """
    :return: Total number of trajectories that can go into the target
    """
    logging.info("Calculating total number of trajectories that can go into the target...")

    x_min, x_max, y_min, y_max = read_input("puzzle_input.txt")
    v_x_max = x_max
    v_x_min = int(sqrt(1 + 8 * x_min) / 2 - 0.5)
    v_y_max = abs(y_min) - 1
    v_y_min = y_min

    set_vy_vx = set()
    for v_y in range(v_y_min, v_y_max + 1):
        for v_x in range(v_x_min, v_x_max + 1):
            if check_trajectory(v_y, v_x, x_min, x_max, y_min, y_max):
                set_vy_vx.add((v_x, v_y))

    no_trajectories = len(set_vy_vx)

    logging.info(f"Solution to part 2 is {no_trajectories}")
    return no_trajectories


if __name__ == '__main__':
    part_1()
    part_2()
