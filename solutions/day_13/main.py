"""
Day 13 Exercise - Transparent Origami
"""
from time import monotonic_ns
from dataclasses import dataclass
from collections.abc import Callable
from typing import NoReturn
import logging
import sys

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("Transparent_Origami.log"),
        logging.StreamHandler(sys.stdout)
    ]
)


def timer_func(func) -> Callable:
    """
    Timer for functions
    :param func: a function to be timed
    :return: a decorator for timing the function
    """
    def wrap_func(*args, **kwargs):
        t_1 = monotonic_ns()
        result = func(*args, **kwargs)
        t_2 = monotonic_ns()
        logging.info(f'Function {func.__name__!r} '
                     f'executed in {(t_2 - t_1):,d}ns')
        return result
    return wrap_func


def read_input(puzzle_file: str) -> (list[list], list[list]):
    """
    Reads puzzle input
    :param puzzle_file: name of puzzle_input file
    :return: cave graph as a nx.Graph object
    """
    with open(puzzle_file, "r", encoding="utf-8") as file:
        puzzle_input = file.read()
    puzzle_input = puzzle_input.split("\n\n")
    dots = puzzle_input[0].split("\n")
    folds = puzzle_input[1].split("\n")
    # converting dots into integer coordinates
    dots = [[int(dot[0]), int(dot[1])] for dot in [dot.split(",") for dot in dots]]
    # converting folds into a comfortable format
    folds = [[fold[11], int(fold[13:])] for fold in folds]
    return dots, folds


@dataclass()
class Paper:
    """
    A Class representing a piece of paper
    ...
    Attributes
    ----------
    dots: list[list]
        a list of coordinates of the dots on the piece of paper

    Methods
    -------
    fold(axis: str, axis_value: int) -> NoReturn
        folds the piece of paper along the axis_value
        in the vertical or horizontal direction (axis)

    no_dots()
        returns the number of visible dots on the paper

    show_paper()
        returns a visualisation of the dots on the piece of paper
    """
    dots: list[list]

    def fold(self, axis: str, axis_value: int) -> NoReturn:
        """
        Folds the piece of paper along the axis_value
        :param axis: "x" - fold horizontally or "y" - fold vertically
        :param axis_value: location of axis along which the fold occurs
        :return: NoReturn
        """
        if axis == "y":
            for dot_index, dot in enumerate(self.dots):
                if dot[1] < axis_value:
                    continue
                self.dots[dot_index][1] = 2 * axis_value - dot[1]
        elif axis == "x":
            for dot_index, dot in enumerate(self.dots):
                if dot[0] < axis_value:
                    continue
                self.dots[dot_index][0] = 2 * axis_value - dot[0]
        else:
            raise Exception("Only x and y axis allowed")

    def no_dots(self):
        """
        :return: total number of dots on the page (overlapping dots count as one)
        """
        tuple_dots = [tuple(dot) for dot in self.dots]
        return len(set(tuple_dots))

    def show_paper(self):
        """
        visualises a pattern made by folding paper in terminal
        :returns a string representing piece of paper
        """
        tuple_dots = [tuple(dot) for dot in self.dots]
        x_max = sorted(tuple_dots, key=lambda x: x[0])[-1][0]
        y_max = sorted(tuple_dots, key=lambda x: x[1])[-1][1]
        paper_visual = ""
        for y_coord in range(y_max + 1):
            for x_coord in range(x_max + 1):
                if (x_coord, y_coord) in tuple_dots:
                    paper_visual += "##"
                else:
                    paper_visual += "  "
            paper_visual += "\n"
        return paper_visual


@timer_func
def part_1() -> int:
    """
    :return:   Total number of dots after one fold
    """
    logging.info("Calculating the total number of dots")
    dots, folds = read_input("puzzle_input.txt")

    fold_1 = folds[0]
    page_1 = Paper(dots)
    page_1.fold(fold_1[0], fold_1[1])
    number_of_dots = page_1.no_dots()

    logging.info(f"The total number of dots after one fold {number_of_dots}")

    return number_of_dots


@timer_func
def part_2() -> str:
    """
    :return: a visualisation of the paper after all the folds
    """
    logging.info("Calculating the total number of dots")
    dots, folds = read_input("puzzle_input.txt")

    page_1 = Paper(dots)
    for fold_instruction in folds:
        page_1.fold(fold_instruction[0], fold_instruction[1])
    folded_paper = page_1.show_paper()

    logging.info(f"A Visualisation of the folded paper: \n {folded_paper}")

    return folded_paper


if __name__ == '__main__':
    print(f"Solution to part 1: {part_1()}")
    print(f"Solution to part 2: \n {part_2()}")
