"""
Solutions to Day 1 exercises
"""
from time import monotonic_ns
import logging
import sys

logging.basicConfig(
    level=logging.DEBUG,
    format="%(asctime)s [%(levelname)s] %(message)s",
    handlers=[
        logging.FileHandler("day_1.log"),
        logging.StreamHandler(sys.stdout)
    ]
)


def timer_func(func):
    """
    Timer for functions
    :param func: function to measure
    :return: decorator for timing functions
    """
    def wrap_func(*args, **kwargs):
        t_1 = monotonic_ns()
        result = func(*args, **kwargs)
        t_2 = monotonic_ns()
        logging.info(f'Function {func.__name__!r} '
                     f'executed in {(t_2 - t_1):,d}ns')
        return result

    return wrap_func


@timer_func
def part_1() -> int:
    """
    :return: total number of increases
    """

    with open("puzzle_input.txt", "r", encoding="utf-8") as file:
        v_1 = [int(i) for i in file.read().splitlines()]

    # calculating the number of increases as difference between two vectors
    number_of_increase = [1 for i, j in zip(v_1, v_1[1:]) if j-i > 0]
    return sum(number_of_increase)


def part_2() -> int:
    """
    :return: total number of increases over the running sum
    """

    with open("puzzle_input.txt", "r", encoding="utf-8") as file:
        v_1 = [int(i) for i in file.read().splitlines()]

    # Calculate running sum
    v_rs = [i + j + l for i, j, l in zip(v_1, v_1[1:], v_1[2:])]
    number_of_increase_rs = [1 for i, j in zip(v_rs, v_rs[1:]) if j - i > 0]

    return sum(number_of_increase_rs)


if __name__ == '__main__':
    print(f"PART 1: The total number of increases is: {part_1()}.")
    print(f"PART 2: The total number of running sum increases is: {part_2()}")
