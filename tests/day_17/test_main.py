from solutions.day_17.main import read_input, check_trajectory
from math import sqrt

# read file
x_min, x_max, y_min, y_max = read_input("tests/day_17/small_target.txt")

def test_check_trajectory():
    # part 1
    v_y_max = abs(y_min + 1)
    highest_point = v_y_max * (v_y_max + 1) / 2
    assert highest_point == 45

    # part 2
    v_x_max = x_max
    v_x_min = int(sqrt(1 + 8 * x_min) / 2 - 0.5)
    v_y_max = abs(y_min) - 1
    v_y_min = y_min

    set_vy_vx = set()
    for v_y in range(v_y_min, v_y_max + 1):
        for v_x in range(v_x_min, v_x_max + 1):
            if check_trajectory(v_y, v_x, x_min, x_max, y_min, y_max):
                set_vy_vx.add((v_x, v_y))

    no_trajectories = len(set_vy_vx)

    assert no_trajectories == 112
