from solutions.day_15.main import read_input, extend_input, calc_total_risk

# read files and extend
small = read_input("tests/day_15/small_chitons.txt")
medium = extend_input("tests/day_15/small_chitons.txt")


def test_calc_total_risk():
    assert calc_total_risk(small) == 40
    assert calc_total_risk(medium) == 315
