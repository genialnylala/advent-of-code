from solutions.day_16.main import read_input, Parser

# read file for sum
sum_1 = read_input("tests/day_16/sum_1.txt")
sum_2 = read_input("tests/day_16/sum_2.txt")
sum_3 = read_input("tests/day_16/sum_3.txt")
sum_4 = read_input("tests/day_16/sum_4.txt")

# read file for operate
op_1 = read_input("tests/day_16/op_1.txt")
op_2 = read_input("tests/day_16/op_2.txt")
op_3 = read_input("tests/day_16/op_3.txt")
op_4 = read_input("tests/day_16/op_4.txt")
op_5 = read_input("tests/day_16/op_5.txt")
op_6 = read_input("tests/day_16/op_6.txt")
op_7 = read_input("tests/day_16/op_7.txt")
op_8 = read_input("tests/day_16/op_8.txt")

def test_sum_versions():
    """Tests Part 1"""
    parser_1 = Parser(sum_1)
    parser_2 = Parser(sum_2)
    parser_3 = Parser(sum_3)
    parser_4 = Parser(sum_4)
    assert parser_1.parse_transmission().sum_versions() == 16
    assert parser_2.parse_transmission().sum_versions() == 12
    assert parser_3.parse_transmission().sum_versions() == 23
    assert parser_4.parse_transmission().sum_versions() == 31

def test_operate():
    """Tests Part 2"""
    parser_1 = Parser(op_1)
    parser_2 = Parser(op_2)
    parser_3 = Parser(op_3)
    parser_4 = Parser(op_4)
    parser_5 = Parser(op_5)
    parser_6 = Parser(op_6)
    parser_7 = Parser(op_7)
    parser_8 = Parser(op_8)
    assert parser_1.parse_transmission().operate() == 3
    assert parser_2.parse_transmission().operate() == 54
    assert parser_3.parse_transmission().operate() == 7
    assert parser_4.parse_transmission().operate() == 9
    assert parser_5.parse_transmission().operate() == 1
    assert parser_6.parse_transmission().operate() == 0
    assert parser_7.parse_transmission().operate() == 0
    assert parser_8.parse_transmission().operate() == 1
