from solutions.day_14.main import read_input, Polymer

starter, rules = read_input("tests/day_14/small_polymer.txt")
my_polymer = Polymer(starter, rules)

def test_Polymer():
    my_polymer.polymerize(10)
    assert my_polymer.calc_diff_max_min() == 1588
    my_polymer.polymerize(30)
    assert my_polymer.calc_diff_max_min() == 2188189693529
