from solutions.day_13.main import Paper, read_input

small_paper = read_input("tests/day_13/small_paper.txt")
small_page = Paper(small_paper[0])

def test_no_dots():
    for fold in small_paper[1]:
        small_page.fold(fold[0],fold[1])
    assert small_page.no_dots() == 16
